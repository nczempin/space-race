class "AI"{}

TURN_IMPULSE = 4000
wp = {}
wpInside = {}
wpOutside = {}

function AI:__init(level)
    self:initWaypoints(level)
end


function AI:updateAI(dt)

    for i,v in pairs(g.shipVec) do
        if i ~= 1 then
            local ship = g.shipVec[i]
            local me = ship.shipEntity.b
            local move = AI:AI(ship,dt)
            if move == SHIP_AI_BRAKE then
                me:applyForce(-math.sin(me:getAngle() - math.pi * 2) * 500 * dt, math.cos(me:getAngle() - math.pi * 2) * 500 * dt)
            elseif move == SHIP_AI_THRUST then
                me:applyForce(math.sin(me:getAngle() - math.pi * 2) * 2000 * dt, -math.cos(me:getAngle() - math.pi * 2) * 2000 * dt)
            elseif move == SHIP_AI_TURN_RIGHT then
                me:applyAngularImpulse(TURN_IMPULSE * dt)
            elseif move == SHIP_AI_TURN_LEFT then
                me:applyAngularImpulse(-TURN_IMPULSE * dt)
            elseif move == SHIP_AI_DO_NOTHING then
            elseif move == SHIP_AI_COUNTER_TURN then
                local av = me:getAngularVelocity()
                if (math.abs(av)>0.1) then
                    if ( av<0) then
                        me:applyAngularImpulse(160 * dt)
                    else
                        me:applyAngularImpulse(-160 * dt)
                    end
                else
                    me:applyAngularImpulse(-av*dt)
                end
            end
        end
    end
end

function AI:AI(ship,dt)
    local val = AI:doAI(ship,dt)
    if val == SHIP_AI_TURN_LEFT or val == SHIP_AI_TURN_RIGHT then
    --self.accelerate = 2
    end
    return val
end
MAX_DISTANCE = 163840 --this is 10x the x size of the default level, an arbitrary "safe" maximum. TODO: find this value based on the level
function AI:doAI(ship, dt)
    local me = ship.shipEntity.b
    local x, y = me:getX(),me:getY()
    local player = ship.shipEntity.player
    local wpIndex = player.nextWaypoint
    local currentWp = wp[wpIndex]

    local dist2Wp = getDistance(x,y,currentWp[1],currentWp[2])
    if (dist2Wp < 800)then
        -- print ("waypoint reached: "..wpIndex.." @ ".. currentWp[1]..", "..currentWp[2])
        player:incrementWaypoint()
    end
    --check for closest 2 waypoints
    --TODO use more efficient data structures / algorithm, check less frequently
    --    local wpi1, wpi2
    --    local d1 = MAX_DISTANCE
    --    local d2 = MAX_DISTANCE
    --    for i=1, #wp do
    --        local d = getDistance(x,y,wp[i][1],wp[i][2])
    --        if (d<d1) then
    --            -- print(".1")
    --            d2 = d1
    --            d1 = d
    --            wpi2 = wpi1
    --            wpi1 = i
    --        elseif (d<d2)then
    --            -- print (".2")
    --            d2 = d
    --            wpi2 = i
    --        end
    --    end
    --    if (wpi1 >=wpi2 or wpi2-wpi1>#wp/2)then
    --        if (ship.nextWaypoint ~= wpi1 )then
    --            ship.nextWaypoint = wpi1
    --            --print ("picked next waypoint: "..wpi1.."@".. currentWp[1]..", "..currentWp[2])
    --        end
    --    end
    --  end


    local playerShip = g.shipVec[1].b
    --local wantX, wantY= playerShip:getX(), playerShip:getY()
    currentWp = wp[ship.shipEntity.player.nextWaypoint]
    local wantX, wantY = currentWp[1],currentWp[2]
    local dirX, dirY = me:getLinearVelocity( )
    local dir = getDistance(dirX,dirY,0,0)
    local deltaX = (x - wantX)+dirX
    local deltaY = (y - wantY)+dirY

    wantAngle =  -math.atan2 (deltaX, deltaY)--math.rad(90)




    --left, right, top, bottom
    --  if self.accelerate > 0 then
    --    self.accelerate = self.accelerate - 1
    --    return SHIP_AI_THRUST
    --  end

    local walls = level:detectWalls(x, y)
    local distLeft = getDistance(x, y, walls[1] * 32, y)
    local distRight = getDistance(x, y, walls[2] * 32, y)
    local distTop = getDistance(x, y, x, walls[3] * 32)
    local distBottom = getDistance(x, y, x, walls[4] * 32)
    local angle = me:getAngle()

    angleDelta = wantAngle-angle
    if (angleDelta>math.pi)then
        angleDelta = angleDelta - math.pi*2
    end
    if (angleDelta < -math.pi)then
        angleDelta = angleDelta + math.pi*2
    end
    if (math.abs(angleDelta)<=math.pi/10)then
        return SHIP_AI_THRUST
    elseif (dir>50 and math.abs(angleDelta)>=3*math.pi/4)then
        return SHIP_AI_BRAKE
    end
    --  else
    if (angleDelta>0 ) then
        return SHIP_AI_TURN_RIGHT
    else
        return SHIP_AI_TURN_LEFT
    end
    --  end

    return self.turnOrSpeed(dir, SHIP_AI_DO_NOTHING)
end

function AI:initWaypoints(level)
    print (level)

    local bresenham = require 'ai/bresenham/bresenham'

    local function findImpact(x0,y0,x1,y1)
        local floor = math.floor
        local hitX = -1
        local hitY = -1
        --  local B = bresenham.los(x0/64,y0/64,floor(x1/64),floor(y1/64), function(x,y)
        local B = bresenham.los(x0,y0,x1,y1, function(x,y)
            --local B = bresenham.los(2,6,6,6, function(x,y)
            if level:get(x,y) == 1 then
                --  if map[x][y] == '#' then
                hitX = x
                hitY = y
                return  false
            end
            return true
        end)

        if B then
            return {-1,-1}
        else
            return {hitX * 32, hitY*32}
        end
    end

    local sp = level:getStartPosition()
    local steps = 64
    local angleDelta = 2* math.pi / steps
    local length = 16000
    for i=0, steps-1 do
        local angle = angleDelta*i-- + math.pi/2
        local xxx = length * math.cos(angle)
        local yyy = -length * math.sin(angle)
        local impact = findImpact(256+xxx/32,256+yyy/32,256,256)
        --        if (i<3) then
        --            print (impact[1] .. ", " .. impact[2])
        --        end
        if (impact[1]>0)then
            wpOutside[i] = impact
            --if (i<3) then
            --            print (i..": "..impact[1] .. ", " .. impact[2])
            --end
        end
        local impact = findImpact(256,256,256+xxx/32,256+yyy/32)
        if (impact[1]>0)then
            wpInside[i] = impact
            --if (i<3) then
            --            print (i..": "..impact[1] .. ", " .. impact[2])
            --end
        end
    end
    for i=1, math.min(#wpInside,#wpOutside) do
        --average inside and outside: TODO: weight towards the inside
        --        print (i, wpInside[i][1], wpInside[i][2])
        --        print (i, wpOutside[i][1], wpOutside[i][2])
        wp[i] = {}
        wp[i][1] = (wpInside[i][1]+wpOutside[i][1])/2
        wp[i][2] = (wpInside[i][2]+wpOutside[i][2])/2
    end
end

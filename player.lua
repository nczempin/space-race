ShipCategory = 10

class "Player"{}

function Player:__init(ship)
    -- relevant only for human player
    self.nextKeyUpdate = 0
    -- relevant only for ai player
    self.nextWaypoint = 1
end
function Player:apply_retro_thrusters(ps)
    ps.b:applyForce(-math.sin(ps.b:getAngle() - math.pi * 2) * 100, math.cos(ps.b:getAngle() - math.pi * 2) * 100)
end
function Player:apply_main_thrusters(dt, ps)
    ps.fuel = ps.fuel - (dt * (100 - ps.health) / 20)
    ps.speed = ps.speed + dt * 100
    if ps.fuel < 0 then
        ps.fuel = 0
    end
    ps.b:applyForce(math.sin(ps.b:getAngle() - math.pi * 2) * 500, -math.cos(ps.b:getAngle() - math.pi * 2) * 500)
end

function Player:turn_right(ps)
    ps.b:applyTorque(4000)
end
function Player:turn_left(ps)
    ps.b:applyTorque(-4000)
end
function Player:update(dt)
    for i=1, #g.shipVec do
        if i==1 then
            humanPlayer:updateHumanPlayerMovement(dt)
        else
            AI:updateAI(dt)
        end
    end
end
function Player:updateHumanPlayerMovement(dt)
    self.nextKeyUpdate = self.nextKeyUpdate - dt
    if self.nextKeyUpdate <= 0 and playerShip.shipEntity.fuel > 0 then

        --TODO: move the keyboard specifics to input.lua
        --TODO: present a unified control interface for human player and ai player

        local ps = playerShip.shipEntity
        -- do human player
        if love.keyboard.isDown("s") then
            Player:apply_retro_thrusters(ps)
        elseif love.keyboard.isDown("w") then
            Player:apply_main_thrusters(dt,ps)
        end
        if love.keyboard.isDown("d") then
            Player:turn_right(ps)
        elseif love.keyboard.isDown("a") then
            Player:turn_left(ps)
        end
        self.nextKeyUpdate = 1 / 600
    end

end

function Player:incrementWaypoint()
    self.nextWaypoint = self.nextWaypoint + 1
    if (self.nextWaypoint > #wp)then
        self.nextWaypoint = 1
    end
end
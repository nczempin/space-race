// alphaFeas001.sce
clear
stacksize(100000000);
cd C:\_trip\munich\miniGameJam\scilab


// Init display params

displ.size = 512;
displ.width = displ.size;
displ.height = displ.size;

levMax = 200.1;

background(displ.height,displ.width,3) = 0;
background(1:displ.height,1:displ.width,1:3) = -levMax*.2;

// Add object

obj1.size = floor(.3*displ.size);
obj1.xos = floor(.5*displ.size);
obj1.yos = floor(.5*displ.size);
obj1.thickness = 10;


T=floor(%pi*2*obj1.size*2);
thhalf = obj1.thickness/2;
radMin = 1-thhalf/obj1.size;
oversample = 1;
S = floor(obj1.thickness*oversample);

at1 = .5;
bt0 = 1-at1;
thickFactor = 1;
randomThicknessMean = 1;
randomThicknessStd = .1;
for t=1:T
    rndnum=grand(1,1,'nor',randomThicknessMean,randomThicknessStd)
    thickFactor = bt0*rndnum + at1*thickFactor;
    // circle example
    for s=1:S
        radFactor = 1 + (s/oversample-thhalf)/obj1.size;
        radFactor = radFactor*thickFactor;
        trigArg = %pi*2.0*t/T;
        xt = floor( obj1.xos + obj1.size*(radFactor*(cos(trigArg)+0.09*cos(3*trigArg))) );
        yt = floor( obj1.xos + obj1.size*(radFactor*(sin(trigArg)+0.09*cos(3*trigArg))) ); 
        background(yt,xt,1) = levMax;
        background(yt,xt,2) = levMax;
        background(yt,xt,3) = levMax;    
//        disp('t=');disp(t);disp('s=');disp(s);disp(xt);disp(yt);
    end 
end

// Anti-alias:  2-d filter

// Non-causal horizontal filter:
a1 = .85;
b0 = 1-a1;
for y=1:displ.height-1
    filterout(1) = background(y,1,1);
    filterout(2) = background(y,1,2);
    filterout(3) = background(y,1,3);
    for x=1:displ.width-1
        filterout(1) = background(y,x,1)*b0 + filterout(1)*a1;
        filterout(2) = background(y,x,2)*b0 + filterout(2)*a1;
        filterout(3) = background(y,x,3)*b0 + filterout(3)*a1;
        background(y,x,1) = filterout(1);
        background(y,x,2) = filterout(2);
        background(y,x,3) = filterout(3);
    end
    filterout(1) = background(y,displ.width,1);
    filterout(2) = background(y,displ.width,2);
    filterout(3) = background(y,displ.width,3);    
    for x=displ.width:2
        filterout(1) = background(y,x,1)*b0 + filterout(1)*a1;
        filterout(2) = background(y,x,2)*b0 + filterout(2)*a1;
        filterout(3) = background(y,x,3)*b0 + filterout(3)*a1;
        background(y,x,1) = filterout(1);
        background(y,x,2) = filterout(2);
        background(y,x,3) = filterout(3);
    end
end


// Non-causal vertical filter:
a1 = .85;
b0 = 1-a1;
for x=1:displ.width-1
    filterout(1) = background(y,1,1);
    filterout(2) = background(y,1,2);
    filterout(3) = background(y,1,3);
    for y=1:displ.height-1
        filterout(1) = background(y,x,1)*b0 + filterout(1)*a1;
        filterout(2) = background(y,x,2)*b0 + filterout(2)*a1;
        filterout(3) = background(y,x,3)*b0 + filterout(3)*a1;
        background(y,x,1) = filterout(1);
        background(y,x,2) = filterout(2);
        background(y,x,3) = filterout(3);
    end
    filterout(1) = background(y,displ.width,1);
    filterout(2) = background(y,displ.width,2);
    filterout(3) = background(y,displ.width,3);    
    for y=displ.height:2
        filterout(1) = background(y,x,1)*b0 + filterout(1)*a1;
        filterout(2) = background(y,x,2)*b0 + filterout(2)*a1;
        filterout(3) = background(y,x,3)*b0 + filterout(3)*a1;
        background(y,x,1) = filterout(1);
        background(y,x,2) = filterout(2);
        background(y,x,3) = filterout(3);
    end
end







// Now limit to between -1 and 1

background = min(background,1);
background = max(background,-1);

// Anti-alias:  2-d filter
a1 = .75;
b0 = 1-a1;
for y=1:displ.height-1
    filterout(1) = background(y,1,1);
    filterout(2) = background(y,1,2);
    filterout(3) = background(y,1,3);
    for x=1:displ.width-1
        filterout(1) = background(y,x,1)*b0 + filterout(1)*a1;
        filterout(2) = background(y,x,2)*b0 + filterout(2)*a1;
        filterout(3) = background(y,x,3)*b0 + filterout(3)*a1;
        background(y,x,1) = filterout(1);
        background(y,x,2) = filterout(2);
        background(y,x,3) = filterout(3);
    end
    filterout(1) = background(y,displ.width,1);
    filterout(2) = background(y,displ.width,2);
    filterout(3) = background(y,displ.width,3);    
    for x=displ.width:2
        filterout(1) = background(y,x,1)*b0 + filterout(1)*a1;
        filterout(2) = background(y,x,2)*b0 + filterout(2)*a1;
        filterout(3) = background(y,x,3)*b0 + filterout(3)*a1;
        background(y,x,1) = filterout(1);
        background(y,x,2) = filterout(2);
        background(y,x,3) = filterout(3);
    end
end




imshow(background);


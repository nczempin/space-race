vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords)
{
	vec2 pSize = vec2(1.0 / 1280.0, 1.0 / 720.0);
	vec4 col = Texel(texture, texture_coords);
	col = col + Texel(texture, vec2(texture_coords.x, texture_coords.y - pSize.y));
	col = col + Texel(texture, vec2(texture_coords.x, texture_coords.y + pSize.y));
	col = col + Texel(texture, vec2(texture_coords.x, texture_coords.y - pSize.y * 2));
	col = col + Texel(texture, vec2(texture_coords.x, texture_coords.y + pSize.y * 2));
	col = col + Texel(texture, vec2(texture_coords.x, texture_coords.y - pSize.y * 3));
	col = col + Texel(texture, vec2(texture_coords.x, texture_coords.y + pSize.y * 3));
	col = col / 7.0;
	return vec4(col.rgb, 1.0);
}
require "player"
require "shipEntity"

ShipCategory = 10

class "Ship"{}

SHIP_AI_THRUST = 1
SHIP_AI_BRAKE = 2
SHIP_AI_TURN_LEFT = 3
SHIP_AI_TURN_RIGHT = 4
SHIP_AI_DO_NOTHING = 5
SHIP_AI_COUNTER_TURN = 6
SHIP_AI_SHIP_SIZE = 64

function Ship:__init(shipEntity)
    self.shipEntity = shipEntity

    print ("shipEntity = " .. self.shipEntity.id)
    g.shipVec[self.shipEntity.id] = self
    self.img = love.graphics.newImage(GFX_PATH.."racer" .. self.shipEntity.id % 4 + 1 .. ".png")
    --g.shipVec[self.id].img:setFilter("nearest", "nearest")
    self.imgBoost = love.graphics.newImage(GFX_PATH.."boost.png")
    self.imgFire = love.graphics.newImage(GFX_PATH.."fire.png")


    self.boost = love.graphics.newParticleSystem(self.imgBoost, 99)
    self.boost:setEmissionRate(20)
    self.boost:setParticleLifetime(2, 2)
    self.boost:setSpeed(20, 25)
    self.boost:setSizes(3, 1)
    self.boost:setColors(255, 255, 255, 63, 255, 255, 255, 0)
    self.boost:start();
    local id = self.shipEntity.id
    g.shipVec[id].crash = love.graphics.newParticleSystem(self.imgFire, 99)
    g.shipVec[id].crash:setEmissionRate(20)
    g.shipVec[id].crash:setParticleLifetime(0.5, 1)
    g.shipVec[id].crash:setSpread(2*3.14)
    g.shipVec[id].crash:setSpeed(50, 0)
    g.shipVec[id].crash:setSizes(0.1, 0.1, 2)
    g.shipVec[id].crash:setSpin(math.pi)
    g.shipVec[id].crash:setColors(255, 127, 0, 255, 255, 127, 0, 127, 255, 255, 255, 0)
    g.shipVec[id].crash_time = 2.0
end


function Ship:getOffset()
    return {-1*self.shipEntity.b:getX()+love.graphics.getWidth()/2,-1*self.shipEntity.b:getY()+love.graphics.getHeight()/2}
end


function updateShipAnimations(dt)
    for i = 1, g.numplayers do
        --g.shipVec[i].boost:setDirection(g.shipVec[i].b:getAngle() + math.pi / 2)
        --g.shipVec[i].boost:setPosition(g.shipVec[i].b:getX(), g.shipVec[i].b:getY())
        local s = g.shipVec[i]
        local se = s.shipEntity
        s.boost:setPosition(0, 0)
        if i == 1 then
            s.boost:setSpeed(se.speed, se.speed * 0.2)
            s.boost:setColors(255, 255, 255, math.min(se.speed * 3, 63), 255, 255, 255, 0)
        end
        s.boost:update(dt)
        --s.crash:setPosition(s.b:getX(), s.b:getY())
        s.crash:update(dt)
        if s.crash_time > 0 then
            s.crash_time = s.crash_time - dt
        else
            s.crash_time = 0
            s.crash:stop()
        end
    end
end

function drawShips(o)
    love.graphics.push()
    love.graphics.translate(o[1],o[2])
    for i = 1, g.numplayers do
        local ship = g.shipVec[i]
        local b = ship.shipEntity.b

        if i == 1 then
            love.graphics.setColor(255, 0, 0, 31)
        else
            love.graphics.setColor(0, (i / #g.shipVec) * 255, 255,31)
        end
        local screenX = b:getX()
        local screenY = b:getY()
        love.graphics.circle("fill", screenX, screenY, ship.shipEntity.s:getRadius())

        --love.graphics.setBlendMode("additive")
        love.graphics.setColor(255, 255, 255)
        --love.graphics.draw(g.shipVec[i].crash, g.shipVec[i].b:getX() + o[1], g.shipVec[i].b:getY() + o[2])
        love.graphics.setBlendMode("alpha")
        love.graphics.draw(ship.img, screenX, screenY, b:getAngle(), 1, 1, 32, 32)
        love.graphics.setBlendMode("additive")
        love.graphics.draw(ship.crash, screenX, screenY)
        love.graphics.draw(ship.boost, screenX, screenY, b:getAngle() + math.pi / 2, 1, 1, -20, 0)
        love.graphics.setBlendMode("alpha")
    end
    love.graphics.pop()
end

-- HUD
class "HUD" {
	x = 0;
	y = 0;
	scale = 1.0;
}

function HUD:__init(x, y, scale)
	self.x = x
	self.y = y
	self.scale = scale
end

function HUD:draw(health, fuel)
    -- Health
    love.graphics.setColor(0, 0, 0, 192)
    love.graphics.rectangle("fill", self.x, self.y, 256, 24)
    if health > 50 then
        love.graphics.setColor(127, 255, 127)
    elseif health > 25 then
        love.graphics.setColor(255, 255, 127)
    else
        love.graphics.setColor(255, 127, 127)
    end
    love.graphics.rectangle("fill", self.x + 2, self.y + 2, health * 2.56 - 4, 24 - 4)
    love.graphics.setColor(255, 255, 255, 191)
    love.graphics.print("Player 1 (" .. math.floor(health) .. "%)", self.x + 16, self.y + 4)
    -- Fuel
    love.graphics.setColor(0, 0, 0, 192)
    love.graphics.rectangle("fill", self.x, self.y + 24, 192, 10)
    love.graphics.setColor(255, 127, 255)
    love.graphics.rectangle("fill", self.x + 2, self.y + 24, fuel * 1.92 - 4, 8)
end
class "Graphics" {
}

function Graphics:__init()
    -- Init Buffer for Postshader
    self.renderBuffer = love.graphics.newCanvas(SCREEN_WIDTH, SCREEN_HEIGHT)
    self.backBuffer = love.graphics.newCanvas(SCREEN_WIDTH, SCREEN_HEIGHT)

    -- Shader
    self.eBlurV = love.graphics.newShader(GFX_PATH.."blurv.glsl")
    self.eBlurJ = love.graphics.newShader(GFX_PATH.."blurh.glsl")
    self.eContrast = love.graphics.newShader(GFX_PATH.."contrast.glsl")

    -- Image Font
    local font = love.graphics.newImageFont(GFX_PATH.."font.png",
        " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,!?-+/():;%&`'*#=[]\"")
    love.graphics.setFont(font)

    --background of stars
    self.bg = love.graphics.newImage(GFX_PATH.."bg.png")
    self.bgQuad = love.graphics.newQuad(0, 0, love.graphics.getWidth() * 32, love.graphics.getHeight() * 32, 512, 512)
    self.bg:setWrap('repeat','repeat')

end

function Graphics:draw()
    love.graphics.setCanvas(self.renderBuffer)
    love.graphics.setShader()
    love.graphics.setColor(255, 255, 255, 255)
    self.cameraPosition = g.shipVec[cameraShip]:getOffset() -- TODO: allow cameras independent of ships
    love.graphics.draw(self.bg, self.bgQuad, self.cameraPosition[1] / 4, self.cameraPosition[2] / 4)
    level:drawBarrier(self.cameraPosition)
    asteroids:draw(self.cameraPosition)
    drawShips(self.cameraPosition)
    startGate:draw(self.cameraPosition)
    minimap:draw()
    drawDebug(self.cameraPosition)
    hud:draw(playerShip.shipEntity.health, playerShip.shipEntity.fuel)

    if (g.paused)then
        love.graphics.print("Press SPACE to unpause.",100,400,0,4,4)
    end

    -- Blur Shader
    love.graphics.setCanvas(self.backBuffer)

    love.graphics.setShader(self.eBlurJ)
    love.graphics.draw(self.renderBuffer)

    love.graphics.setShader(self.eBlurV)
    love.graphics.draw(self.backBuffer)

    love.graphics.setShader(self.eContrast)
    love.graphics.draw(self.backBuffer)

    love.graphics.setCanvas()
    love.graphics.setShader()
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.draw(self.renderBuffer)
    love.graphics.setColor(255, 255, 255, 48)
    love.graphics.draw(self.backBuffer)

end
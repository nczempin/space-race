-- Minimap
class "Minimap" {
    x = 0;
    y = 0;
    scale = 1.0;
}

function Minimap:__init(img, x, y, w, h, scale)
    self.x = x
    self.y = y
    self.w = w
    self.h = h
    self.scale = scale
    self.img = love.graphics.newImage(img)
end

function Minimap:draw(offset)
    love.graphics.setBlendMode("additive")
    love.graphics.setColor(127, 127, 255, 127)
    love.graphics.draw(self.img, self.x, self.y, 0, self.w / self.img:getWidth(), self.w / self.img:getHeight())
    for i, v in pairs(g.shipVec) do
        if i == 1 then
            love.graphics.setColor(255, 0, 0)
        else
            love.graphics.setColor(0, (i / #g.shipVec) * 255, 255)
        end
        love.graphics.circle("fill", self.x + v.shipEntity.b:getX() / (self.w * self.scale), self.y + v.shipEntity.b:getY() / (self.h * self.scale), 4, 16)
        love.graphics.setBlendMode("alpha")
    end
end
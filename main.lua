require "32log"
require "game"
require "level"

require "startgate"
require "ai/ai"

require "audio/audio"
require "graphics/graphics"
require "graphics/debugInfo"
require "graphics/minimap"
require "graphics/hud"

require "ship"
require "asteroid"


require "world"
require "input/input"
require "util"
MAP_FILENAME = "res/scilab/backgrounds/bg001.png"
NUMBER_OF_RACERS = 6
cameraShip = 1
aiPlayer = {}
GFX_PATH = "res/gfx/"

function love.load()

    graphics = Graphics:new()
    level = Level:new(MAP_FILENAME)


    g = Game:new()
    love.physics.setMeter(64)
    world = World:new()
    asteroids = AsteroidField:new()
    level:createMap()
    local gate = level:getStartPosition()

    setup_ships(level,gate)
    startGate = StartGate:new(gate,g.numplayers)
    load_audio()
    minimap = Minimap:new(MAP_FILENAME, love.graphics.getWidth() - 256, 0, 256, 256, 0.25)
    hud = HUD:new(8, 8, 1)
    create_asteroids()


    input = Input:new()
    input:setCameraCallback(changeCameraShip)
end


function setup_ships(level,gate)
    -- set up ships
    local xMiddleOfGate = (gate[3] + gate[1])/2
    for i = 1, NUMBER_OF_RACERS, 1 do

        local gridX = (i-2) %3
        local gridY = (i)/3
        local pos = {xMiddleOfGate+gridX*96, gate[2]+48+gridY*96}
        local player = Player:new()
        if i == 1 then
            print "i==1"
            humanPlayer = Player:new()
            local shipEntity = ShipEntity:new(pos, humanPlayer)
            playerShip = Ship:new(shipEntity)
        else
            print ("i=="..i)

            aiPlayer[i]= Player:new()
            print(level)
            local ai = AI:new(level)

            local shipEntity = ShipEntity:new(pos, aiPlayer[i])
            local s = Ship:new(shipEntity)
        end
    end
end

function love.draw()
    graphics:draw()
end

function changeCameraShip(camera)
    cameraShip = camera --TODO: only allow as many cameras as there are ships
end

function love.update(dt)
    love.window.setTitle("Space Race (FPS:" .. love.timer.getFPS() .. ")")
    if (g.paused)then return end
    input:update()
    world:update(dt)
    updateShipAnimations(dt)
    Player:update(dt)
    playerShip.shipEntity.speed = playerShip.shipEntity.speed - dt * 50
    if playerShip.shipEntity.speed > 25 then
        playerShip.shipEntity.speed = 25
    elseif playerShip.shipEntity.speed < 0 then
        playerShip.shipEntity.speed = 0
    end
    level:update(dt)
    asteroids:update(dt)
    for i,v in pairs(g.shipVec) do
        startGate:update(dt,v.shipEntity:getPos(), i)
    end
end


function love.quit()

end

function beginContact(a,b,coll)
    shipCollideCheckStart(a, b, coll)
end

function endContact(a,b,coll)
    shipCollideCheckEnd(a, b, coll)
end

function preSolve(a,b,coll)
end

function postSolve(a,b,coll)
end

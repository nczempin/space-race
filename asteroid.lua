-- AsteroidField
class "AsteroidField" {
    img = {};
    cnt = 0;
    asteroid = {};
}

function AsteroidField:__init()
    self.img = {}
    self.img[1] = love.graphics.newImage(GFX_PATH.."asteroid_001.png")
    self.img[2] = love.graphics.newImage(GFX_PATH.."asteroid_002.png")
    self.img[3] = love.graphics.newImage(GFX_PATH.."asteroid_003.png")
    self.img[1]:setFilter("nearest", "nearest")
    self.img[2]:setFilter("nearest", "nearest")
    self.img[3]:setFilter("nearest", "nearest")
    self.cnt = 0
end

function AsteroidField:update(dt)

end

function AsteroidField:draw(offset)
    for i = 1, self.cnt do
        self.asteroid[i]:draw(offset)
    end
end

function AsteroidField:createAsteroid(x, y, scale)
    self.cnt = self.cnt + 1
    self.asteroid[self.cnt] = Asteroid:new(x, y, scale)
    self.asteroid[self.cnt].parent = self
end

-- Asteroid
class "Asteroid" {
    x = 0;
    y = 0;
    scale = 1.0;
}

function Asteroid:__init(x, y, scale)
    self.x = x
    self.y = y
    self.scale = scale
    self.red = math.random(31, 191)
    self.green = math.random(0, 127)
    self.blue = math.random(0, 31)
    self.imgNr = math.random(1, 3)

    self.body = love.physics.newBody(world.loveWorld, self.x, self.y, "dynamic")
    self.shape = love.physics.newCircleShape(64 * scale)
    self.fixture = love.physics.newFixture(self.body, self.shape, scale)
    self.body:setAngle(math.random(0, 360))
end
function create_asteroids()
    -- Asteroid Test
    local x, y
    for i = 0, 999 do
        x = math.random(0, (level.image:getWidth() - 1) * level.scaleFactor)
        y = math.random(0, (level.image:getHeight() - 1) * level.scaleFactor)
        if level:get(math.floor(x / level.scaleFactor), math.floor(y / level.scaleFactor)) == 1 then
            asteroids:createAsteroid(x, y, math.random(1, 10) * 0.1)
        end
    end
end

function Asteroid:update()

end

function Asteroid:draw(offset)
    love.graphics.setColor(self.red, self.green, self.blue)
    love.graphics.draw(self.parent.img[self.imgNr], self.body:getX() + offset[1], self.body:getY() + offset[2], self.body:getAngle(), self.scale, self.scale, 64, 64)
end